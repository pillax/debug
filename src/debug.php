<?php

if ( ! function_exists('d')) {
    /**
     * Dump variable
     *
     * @param mixed $var
     */
    function d($var) {
        var_dump($var);
    }
}

if ( ! function_exists('dd')) {
    /**
     * Dump variable and die
     *
     * @param mixed $var
     */
    function dd($var) {
        d($var); die;
    }
}